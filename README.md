# Galley plugin for CakePHP

Provides a small Docker dev environment for [CakePHP](https://cakephp.org/) development.

## Introduction

Galley provides a Docker powered local development experience for CakePHP that is compatible with macOS, Windows (WSL2), and Linux. Other than Docker, no software or libraries are required to be installed on your local computer before using Galley. Galley's simple CLI means you can start building your CakePHP application without any previous Docker experience.

### Inspiration

Galley is inspired by and derived from [Vessel](https://github.com/shipping-docker/vessel) by [Chris Fidao](https://github.com/fideloper) as well as [Laravel Sail](https://github.com/laravel/laravel). If you're looking for a thorough introduction to Docker, check out Chris' course: [Shipping Docker](https://serversforhackers.com/shipping-docker).

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```sh
composer require --dev amayer5125/galley
```

Next [load the plugin](https://book.cakephp.org/5/en/plugins.html#loading-a-plugin) in your src/Application.php file.

```php
$this->addOptionalPlugin('Galley');
```

After you have the plugin installed you can initalize Galley by running the following:

```sh
bin/cake galley install
```

This will create a `docker-compose.yml` file in your project's root directory. After following the directions output by the command, you can start your application by running:

```sh
vendor/bin/galley up -d
```

Head to [http://localhost](http://localhost) in your browser and see your CakePHP site!

### Optional Setup

Instead of repeatedly typing `vendor/bin/galley` to execute Galley commands, you may wish to configure a Bash or ZSH alias that allows you to execute Galley's commands more easily:

```bash
alias galley="vendor/bin/galley"
```

## Usage

To get a list of supported commands and examples run:

```sh
vendor/bin/galley --help
```

## Xdebug Support

Galley comes with [Xdebug](https://xdebug.org/) support out of the box. To enable it create a file named `docker-compose.override.yml` in the root of your project with the following contents:

```yaml
services:
  app:
    environment:
      XDEBUG_MODE: "debug,develop"
    extra_hosts:
      - "host.docker.internal:host-gateway"
```

After creating the file run `galley up -d`. Xdebug should now be enabled.

Make sure the `docker-compose.override.yml` file is ignored from git.
